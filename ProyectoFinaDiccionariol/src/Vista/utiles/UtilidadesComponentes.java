/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.utiles;

import javax.swing.JComboBox;
import Modelo.enums.Idiomas;


public class UtilidadesComponentes {
    public static void cargarComboIdiomas(JComboBox cbx) {
        cbx.removeAllItems();
        for(Idiomas doc : Idiomas.values()) {
            cbx.addItem(doc);
        }
    }

    public static Idiomas obtenerComboIdiomas(JComboBox cbx) {
        return (Idiomas)cbx.getSelectedItem();
    }
    
}
