/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista;

import Controlador.dao.PersonaDao;
import Controlador.dao.PalabraSugeridaDao;
import Vista.ModeloTabla.ModeloTablaAdmin;
import Vista.ModeloTabla.ModeloTablaSugerencia;
import javax.swing.JOptionPane;


public class FrmAdministrador extends javax.swing.JFrame {

    PersonaDao personaDao = new PersonaDao();
    PalabraSugeridaDao palabrasugeridaDao = new PalabraSugeridaDao();
    private ModeloTablaAdmin modelotablaAdmin = new ModeloTablaAdmin();
    private ModeloTablaSugerencia modelotablaSugerencia = new ModeloTablaSugerencia();

    /**
     * Creates new form FrmAdministrador
     */
    public FrmAdministrador() {
        initComponents();
        cargarTablaSurgerencia();
    }
    
    /**
     * Permite cargar la información de administradores en la tabla correspondiente
     */
    public void cargarTablaAdmin() {
        try {
            modelotablaAdmin.setLista(personaDao.getListado());
            tblTablaAdmin.setModel(modelotablaAdmin);
            tblTablaAdmin.getColumnModel().getColumn(0).setMaxWidth(30);
            tblTablaAdmin.getColumnModel().getColumn(1).setMaxWidth(120);
            tblTablaAdmin.getColumnModel().getColumn(2).setMaxWidth(90);
            tblTablaAdmin.getColumnModel().getColumn(3).setMaxWidth(90);
            tblTablaAdmin.getColumnModel().getColumn(4).setMaxWidth(90);
            tblTablaAdmin.updateUI();
        } catch (Exception e) {
        }
    }
    
    /**
     * Permite cargar la información de las palabras sugeridas en la tabla correspondiente 
     */
    private void cargarTablaSurgerencia() {
        try {
            modelotablaSugerencia.setLista(palabrasugeridaDao.listar());
            tblTablaSugerencia.setModel(modelotablaSugerencia);
            tblTablaSugerencia.updateUI();
        } catch (Exception e) {
        }
    }
    
    /**
     * Permite limpiar los campos de texto asignados
     */
    public void limpiar() {
        txtRegistroNombre.setText("");
        txtRegistroUsuario.setText("");
        txtContrasenia.setText("");
        txtConfirmarContrasenia.setText("");
    }
    
    /**
     * Permite guardar el registro de cada administrador
     */
    public void guardar() {
        personaDao.setPersona(null);
        personaDao.getPersona().setNombre(txtRegistroNombre.getText());
        personaDao.getPersona().setRol(txtRegistroRol.getText());
        personaDao.getPersona().getCuenta().setUser(txtRegistroUsuario.getText());
        personaDao.getPersona().getCuenta().setClave(txtContrasenia.getText());
        if (txtContrasenia.getText().equals(txtConfirmarContrasenia.getText())) {
            if (personaDao.getPersona().getId() == null) {
                if (personaDao.guardar()) {
                    JOptionPane.showMessageDialog(this, "Se ha registrado correctamente");
                    limpiar();
                    cargarTablaAdmin();
                } else {
                    JOptionPane.showMessageDialog(this, "No registrado");
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "La confirmación de contraseña no es igual");
        }

    }
    
    /**
     * Permite modificar datos de los administradores
     */
    public void modificar() {
        personaDao.setPersonaAux(null);
        personaDao.getPersonaAux().setNombre(txtRegistroNombre.getText());
        personaDao.getPersonaAux().setRol(txtRegistroRol.getText());
        personaDao.getPersonaAux().getCuenta().setUser(txtRegistroUsuario.getText());
        personaDao.getPersonaAux().getCuenta().setClave(txtContrasenia.getText());
        if (personaDao.getPersona().getNombre() != null) {
            if (personaDao.modificar(personaDao.getPersonaAux())) {
                JOptionPane.showMessageDialog(this, "Modificado correctamente");
                limpiar();
                cargarTablaAdmin();
            } else {
                JOptionPane.showMessageDialog(this, "No modificado");
            }
        }
    }
    
    /**
     * Permite eliminar administradores según la fila seleccionada
     */
    public void eliminar() {
        int fila = (tblTablaAdmin.getSelectedRow());
        try {
            int i = JOptionPane.showConfirmDialog(this, "¿Seguro, quieres eliminar un administrador?", "Alerta", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                personaDao.eliminar(personaDao.listar().consultarDatoPos(fila));
                JOptionPane.showMessageDialog(this, "Eliminado correctamente");
                cargarTablaAdmin();
            } else if (i == 1){
                JOptionPane.showMessageDialog(this, "De acuerdo :)");
            } 
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "No se ha podido eliminar");
        }
    }
    
    /**
     * Permite eliminar palabra sugerida según la fila seleccionada
     */
    public void eliminarPalabra() {
        int fila = (tblTablaSugerencia.getSelectedRow());
        try {
            int i = JOptionPane.showConfirmDialog(this, "¿Seguro, quieres eliminar una palabra sugerida?", "Alerta", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                palabrasugeridaDao.eliminar(palabrasugeridaDao.listar().consultarDatoPos(fila));
                JOptionPane.showMessageDialog(this, "Eliminado correctamente");
                cargarTablaSurgerencia();
            } else if (i == 1){
                JOptionPane.showMessageDialog(this, "De acuerdo :)");
            } 
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "No se ha podido eliminar");
        }
    }
    
    /**
     * Permite buscar un administrador por medio de su nombre ingresado en el campo de texto
     */
    private void buscadorPersona() {
        modelotablaAdmin.setLista(personaDao.buscar(txtRegistroNombre.getText()));
        tblTablaAdmin.setModel(modelotablaAdmin);
        tblTablaAdmin.updateUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlPrincipal = new javax.swing.JPanel();
        lblTituloPrincipal = new javax.swing.JLabel();
        pnlRegistro = new javax.swing.JPanel();
        pnlRegistro1 = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        lblClave = new javax.swing.JLabel();
        lblConfirmarClave = new javax.swing.JLabel();
        txtRegistroUsuario = new javax.swing.JTextField();
        txtContrasenia = new javax.swing.JPasswordField();
        txtConfirmarContrasenia = new javax.swing.JPasswordField();
        btnEliminarAdmin = new javax.swing.JButton();
        lblIconoNuevoUsuario = new javax.swing.JLabel();
        txtRegistroRol = new javax.swing.JTextField();
        lblUsuario1 = new javax.swing.JLabel();
        lblUsuario2 = new javax.swing.JLabel();
        txtRegistroNombre = new javax.swing.JTextField();
        btnRegistrarAdmin = new javax.swing.JButton();
        btnModificarAdmin = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        pnlListaAdministradores = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTablaAdmin = new javax.swing.JTable();
        btnCerrarSesion = new javax.swing.JButton();
        btnPalabras = new javax.swing.JButton();
        btnModificarPalabras = new javax.swing.JButton();
        pnlPalabrasSugeridas = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTablaSugerencia = new javax.swing.JTable();
        btnEliminarPalabras = new javax.swing.JButton();
        lblNombreAdmin = new javax.swing.JLabel();
        lblTituloPrincipal1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        pnlPrincipal.setLayout(null);

        lblTituloPrincipal.setFont(new java.awt.Font("Tempus Sans ITC", 1, 24)); // NOI18N
        lblTituloPrincipal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTituloPrincipal.setText("ADMINISTRACIÓN");
        pnlPrincipal.add(lblTituloPrincipal);
        lblTituloPrincipal.setBounds(0, 10, 870, 33);

        pnlRegistro.setLayout(null);

        pnlRegistro1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Registro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tempus Sans ITC", 0, 11))); // NOI18N
        pnlRegistro1.setLayout(null);

        lblTitulo.setFont(new java.awt.Font("Tempus Sans ITC", 1, 18)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("Nuevo Administrador");
        pnlRegistro1.add(lblTitulo);
        lblTitulo.setBounds(0, 20, 400, 30);

        lblUsuario.setFont(new java.awt.Font("Tempus Sans ITC", 1, 12)); // NOI18N
        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsuario.setText("Usuario:");
        pnlRegistro1.add(lblUsuario);
        lblUsuario.setBounds(40, 210, 120, 17);

        lblClave.setFont(new java.awt.Font("Tempus Sans ITC", 1, 12)); // NOI18N
        lblClave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblClave.setText("Contraseña:");
        pnlRegistro1.add(lblClave);
        lblClave.setBounds(30, 250, 130, 17);

        lblConfirmarClave.setFont(new java.awt.Font("Tempus Sans ITC", 1, 12)); // NOI18N
        lblConfirmarClave.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblConfirmarClave.setText("Confirme Contraseña:");
        pnlRegistro1.add(lblConfirmarClave);
        lblConfirmarClave.setBounds(30, 290, 130, 17);

        txtRegistroUsuario.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        pnlRegistro1.add(txtRegistroUsuario);
        txtRegistroUsuario.setBounds(170, 200, 190, 30);

        txtContrasenia.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        pnlRegistro1.add(txtContrasenia);
        txtContrasenia.setBounds(170, 240, 190, 30);

        txtConfirmarContrasenia.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        pnlRegistro1.add(txtConfirmarContrasenia);
        txtConfirmarContrasenia.setBounds(170, 280, 190, 30);

        btnEliminarAdmin.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnEliminarAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono_eliminar.png"))); // NOI18N
        btnEliminarAdmin.setText("Eliminar");
        btnEliminarAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAdmin(evt);
            }
        });
        pnlRegistro1.add(btnEliminarAdmin);
        btnEliminarAdmin.setBounds(10, 330, 110, 30);

        lblIconoNuevoUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIconoNuevoUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icono_nuevoAdmin.png"))); // NOI18N
        pnlRegistro1.add(lblIconoNuevoUsuario);
        lblIconoNuevoUsuario.setBounds(0, 50, 240, 70);

        txtRegistroRol.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        txtRegistroRol.setText("Administrador");
        txtRegistroRol.setEnabled(false);
        pnlRegistro1.add(txtRegistroRol);
        txtRegistroRol.setBounds(170, 160, 190, 30);

        lblUsuario1.setFont(new java.awt.Font("Tempus Sans ITC", 1, 12)); // NOI18N
        lblUsuario1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsuario1.setText("Rol:");
        pnlRegistro1.add(lblUsuario1);
        lblUsuario1.setBounds(40, 170, 120, 17);

        lblUsuario2.setFont(new java.awt.Font("Tempus Sans ITC", 1, 12)); // NOI18N
        lblUsuario2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUsuario2.setText("Nombre y Apellido:");
        pnlRegistro1.add(lblUsuario2);
        lblUsuario2.setBounds(40, 130, 120, 17);

        txtRegistroNombre.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        txtRegistroNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRegistroNombreKeyTyped(evt);
            }
        });
        pnlRegistro1.add(txtRegistroNombre);
        txtRegistroNombre.setBounds(170, 120, 190, 30);

        btnRegistrarAdmin.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnRegistrarAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono_registrarse.png"))); // NOI18N
        btnRegistrarAdmin.setText("Registrar");
        btnRegistrarAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarAdmin(evt);
            }
        });
        pnlRegistro1.add(btnRegistrarAdmin);
        btnRegistrarAdmin.setBounds(250, 330, 110, 30);

        btnModificarAdmin.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnModificarAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icono_modificar.png"))); // NOI18N
        btnModificarAdmin.setText("Modificar");
        btnModificarAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarAdmin(evt);
            }
        });
        pnlRegistro1.add(btnModificarAdmin);
        btnModificarAdmin.setBounds(130, 330, 110, 30);

        btnBuscar.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/magnifier.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscar(evt);
            }
        });
        pnlRegistro1.add(btnBuscar);
        btnBuscar.setBounds(260, 80, 90, 30);

        pnlRegistro.add(pnlRegistro1);
        pnlRegistro1.setBounds(0, 0, 400, 380);

        pnlPrincipal.add(pnlRegistro);
        pnlRegistro.setBounds(10, 40, 400, 380);

        pnlListaAdministradores.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Lista de Administradores", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tempus Sans ITC", 0, 11))); // NOI18N
        pnlListaAdministradores.setLayout(null);

        tblTablaAdmin.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        tblTablaAdmin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Nombre y apellido", "Usuario", "Contraseña"
            }
        ));
        tblTablaAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblTablaAdminMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblTablaAdmin);
        if (tblTablaAdmin.getColumnModel().getColumnCount() > 0) {
            tblTablaAdmin.getColumnModel().getColumn(0).setMaxWidth(30);
            tblTablaAdmin.getColumnModel().getColumn(1).setMaxWidth(500);
            tblTablaAdmin.getColumnModel().getColumn(2).setMaxWidth(150);
            tblTablaAdmin.getColumnModel().getColumn(3).setMaxWidth(150);
        }

        pnlListaAdministradores.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 420, 140);

        pnlPrincipal.add(pnlListaAdministradores);
        pnlListaAdministradores.setBounds(420, 40, 440, 190);

        btnCerrarSesion.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnCerrarSesion.setText("Cerrar Sesión");
        btnCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarSesionActionPerformed(evt);
            }
        });
        pnlPrincipal.add(btnCerrarSesion);
        btnCerrarSesion.setBounds(10, 10, 140, 30);

        btnPalabras.setFont(new java.awt.Font("Tempus Sans ITC", 1, 11)); // NOI18N
        btnPalabras.setText("Palabras");
        btnPalabras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPalabrasActionPerformed(evt);
            }
        });
        pnlPrincipal.add(btnPalabras);
        btnPalabras.setBounds(280, 450, 120, 30);

        btnModificarPalabras.setFont(new java.awt.Font("Tempus Sans ITC", 1, 11)); // NOI18N
        btnModificarPalabras.setText("Editar Palabras");
        btnModificarPalabras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarPalabrasActionPerformed(evt);
            }
        });
        pnlPrincipal.add(btnModificarPalabras);
        btnModificarPalabras.setBounds(450, 450, 140, 30);

        pnlPalabrasSugeridas.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Palabras Sugeridas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tempus Sans ITC", 0, 11))); // NOI18N
        pnlPalabrasSugeridas.setLayout(null);

        tblTablaSugerencia.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        tblTablaSugerencia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Usuario", "Palabra"
            }
        ));
        jScrollPane2.setViewportView(tblTablaSugerencia);

        pnlPalabrasSugeridas.add(jScrollPane2);
        jScrollPane2.setBounds(10, 20, 420, 110);

        btnEliminarPalabras.setFont(new java.awt.Font("Tempus Sans ITC", 0, 11)); // NOI18N
        btnEliminarPalabras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icono_eliminar.png"))); // NOI18N
        btnEliminarPalabras.setText("Eliminar");
        btnEliminarPalabras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarPalabras(evt);
            }
        });
        pnlPalabrasSugeridas.add(btnEliminarPalabras);
        btnEliminarPalabras.setBounds(320, 140, 110, 30);

        pnlPrincipal.add(pnlPalabrasSugeridas);
        pnlPalabrasSugeridas.setBounds(420, 240, 440, 180);

        lblNombreAdmin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNombreAdmin.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlPrincipal.add(lblNombreAdmin);
        lblNombreAdmin.setBounds(740, 10, 120, 30);

        lblTituloPrincipal1.setFont(new java.awt.Font("Tempus Sans ITC", 1, 14)); // NOI18N
        lblTituloPrincipal1.setText("Administrador@:");
        pnlPrincipal.add(lblTituloPrincipal1);
        lblTituloPrincipal1.setBounds(610, 10, 130, 30);

        getContentPane().add(pnlPrincipal);
        pnlPrincipal.setBounds(0, 0, 880, 510);

        setSize(new java.awt.Dimension(889, 549));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarAdmin(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAdmin
        if (tblTablaAdmin.getSelectedRowCount() > 0) {
            eliminar();
            limpiar();
            cargarTablaAdmin();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un administrador de la tabla", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarAdmin

    private void btnRegistrarAdmin(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarAdmin
        if (txtRegistroNombre.getText().trim().length() > 0 && txtRegistroUsuario.getText().trim().length() > 0) {
            if (!personaDao.UsuarioRepetido(txtRegistroUsuario.getText())) {
                guardar();
                limpiar();
                cargarTablaAdmin();
            } else {
                JOptionPane.showMessageDialog(this, "El nombre de usuario ya está en uso :D", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "LLene todos los campos", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnRegistrarAdmin

    private void btnModificarAdmin(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarAdmin
        if (tblTablaAdmin.getSelectedRowCount() > 0) {
            modificar();
            limpiar();
            cargarTablaAdmin();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione un administrador de la tabla", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnModificarAdmin

    private void btnCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarSesionActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarSesionActionPerformed

    private void btnPalabrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPalabrasActionPerformed
        FrmPalabra fp = new FrmPalabra();
        fp.setVisible(true);
    }//GEN-LAST:event_btnPalabrasActionPerformed

    private void btnEliminarPalabras(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarPalabras
        if (tblTablaSugerencia.getSelectedRowCount() > 0) {
            eliminarPalabra();
            cargarTablaSurgerencia();
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione una palabra de la tabla", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarPalabras

    private void btnModificarPalabrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarPalabrasActionPerformed
        FrmEditarPalabra editarPalabra = new FrmEditarPalabra();
        editarPalabra.setVisible(true);
    }//GEN-LAST:event_btnModificarPalabrasActionPerformed

    private void btnBuscar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscar
        buscadorPersona();
        limpiar();
    }//GEN-LAST:event_btnBuscar

    private void tblTablaAdminMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTablaAdminMouseReleased
        int aux = tblTablaAdmin.getSelectedRow();
        String auxPer = String.valueOf(tblTablaAdmin.getValueAt(aux, 1));
        personaDao.setPersona(null);
        personaDao.setPersona(personaDao.buscarUnaPersona(auxPer));
        txtRegistroNombre.setText(personaDao.getPersona().getNombre());
        txtRegistroUsuario.setText(personaDao.getPersona().getCuenta().getUser());
        txtContrasenia.setText(personaDao.getPersona().getCuenta().getClave());
    }//GEN-LAST:event_tblTablaAdminMouseReleased

    private void txtRegistroNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRegistroNombreKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(rootPane, "Ingrese solo letras", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_txtRegistroNombreKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmAdministrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCerrarSesion;
    private javax.swing.JButton btnEliminarAdmin;
    private javax.swing.JButton btnEliminarPalabras;
    private javax.swing.JButton btnModificarAdmin;
    private javax.swing.JButton btnModificarPalabras;
    private javax.swing.JButton btnPalabras;
    private javax.swing.JButton btnRegistrarAdmin;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblClave;
    private javax.swing.JLabel lblConfirmarClave;
    private javax.swing.JLabel lblIconoNuevoUsuario;
    public static javax.swing.JLabel lblNombreAdmin;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTituloPrincipal;
    private javax.swing.JLabel lblTituloPrincipal1;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblUsuario1;
    private javax.swing.JLabel lblUsuario2;
    public static javax.swing.JPanel pnlListaAdministradores;
    private javax.swing.JPanel pnlPalabrasSugeridas;
    private javax.swing.JPanel pnlPrincipal;
    private javax.swing.JPanel pnlRegistro;
    public static javax.swing.JPanel pnlRegistro1;
    private javax.swing.JTable tblTablaAdmin;
    private javax.swing.JTable tblTablaSugerencia;
    private javax.swing.JPasswordField txtConfirmarContrasenia;
    private javax.swing.JPasswordField txtContrasenia;
    private javax.swing.JTextField txtRegistroNombre;
    private javax.swing.JTextField txtRegistroRol;
    private javax.swing.JTextField txtRegistroUsuario;
    // End of variables declaration//GEN-END:variables
}
