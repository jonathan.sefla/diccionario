
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaModificarPalabraSinonimos extends AbstractTableModel{
    private ListaSimple<String> lista;

    public ListaSimple<String> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<String> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        String sino = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return sino;
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Sinonimo";
            default: return null;
        }
    }
}
