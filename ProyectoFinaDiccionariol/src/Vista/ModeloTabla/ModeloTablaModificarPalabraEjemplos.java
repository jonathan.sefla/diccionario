/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import Modelo.Ejemplo;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaModificarPalabraEjemplos extends AbstractTableModel{
    private ListaSimple<Ejemplo> lista;

    public ListaSimple<Ejemplo> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Ejemplo> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Ejemplo ejemplo = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return ejemplo.getDescripcion();
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Ejemplos";
            default: return null;
        }
    }
}
