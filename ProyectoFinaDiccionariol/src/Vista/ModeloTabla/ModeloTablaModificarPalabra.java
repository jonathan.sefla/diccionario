/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import Modelo.Palabra;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaModificarPalabra extends AbstractTableModel {
    private ListaSimple<Palabra> lista;

    public ListaSimple<Palabra> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Palabra> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Palabra palabra = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return (i+1);
            case 1: return palabra.getNombrePalabra();
            case 2: return (palabra.getTraduccion() != null)? palabra.getTraduccion():"S/N";
            case 3: return palabra.getSignificado();
            case 4: 
                String idiomas = "";
                for (int j = 0; j < palabra.getIdiomas().tamanio(); j++) {
                    idiomas = idiomas+ " " + palabra.getIdiomas().consultarDatoPos(j).getSiglas();
                }
                return idiomas;
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Nro";
            case 1: return "Palabra";
            case 2: return "Traducción";
            case 3: return "Significado";
            case 4: return "Idiomas";
            default: return null;
        }
    }
    
}
