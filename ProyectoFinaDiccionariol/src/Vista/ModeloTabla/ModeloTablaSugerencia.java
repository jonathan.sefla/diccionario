/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import Modelo.PalabraSugerida;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaSugerencia extends AbstractTableModel{
    private ListaSimple<PalabraSugerida> lista;

    public ListaSimple<PalabraSugerida> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<PalabraSugerida> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public int getColumnCount() {
       return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        PalabraSugerida palabraS = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return palabraS.getId();
            case 1: return palabraS.getNombreUsuario();              
            case 2: return palabraS.getPalabra();
            default: return null;
        }  
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Nro";  
            case 1: return "Usuario";
            case 2: return "Palabra";
            default: return null;
        }
    }
    
}
