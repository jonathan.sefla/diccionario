/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import Modelo.Palabra;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaPalabra extends AbstractTableModel {

    ListaSimple<Palabra> lista = new ListaSimple<>();

    public ListaSimple<Palabra> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Palabra> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public int getColumnCount() {
        return 3;

    }

    @Override
    public Object getValueAt(int i, int i1) {
        Palabra palabra = lista.consultarDatoPos(i);
        switch (i1) {
            case 0:
                return palabra.getNombrePalabra();
            case 1:
                return palabra.getTraduccion();
            case 2:
                for (int j = 0; j < palabra.getSinonimo().tamanio(); j++) {
                    return palabra.getSinonimo().consultarDatoPos(j);
                }

            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int i1) {
        switch (i1) {
            case 0:
                return "Palabra";
            case 1:
                return "Traducción";
            case 2:
                return "Sinonimo";

            default:
                return null;
        }
    }

}
