/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Vista.ModeloTabla;

import Controlador.lista.ListaSimple;
import Modelo.Persona;
import javax.swing.table.AbstractTableModel;


public class ModeloTablaAdmin extends AbstractTableModel {
    private ListaSimple<Persona> lista;
    
    public ListaSimple<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Persona persona = lista.consultarDatoPos(i);
        switch(i1) {
            case 0: return (i+1);
            case 1: return persona.getNombre();
            case 2: return persona.getRol();
            case 3: return persona.getCuenta().getUser();
            case 4: return persona.getCuenta().getClave();
            default: return null;
        }        
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "Nro";    
            case 1: return "Nombre y apellido";
            case 2: return "Rol";
            case 3: return "Usuario";
            case 4: return "Contraseña";
            default: return null;
        }
    }

    
    
    

     
}
