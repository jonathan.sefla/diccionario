/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.lista;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class ListaSimple<T> {
    
    public NodoDato cabecera;
    NodoDato presente = new NodoDato();
    NodoDato pasado = new NodoDato();
    public Class<T> clase;

    public ListaSimple() {
        this.cabecera = null;

    }

    public void setClase(Class<T> clase) {
        this.clase = clase;
    }

    public ListaSimple(Class<T> clase) {
        this.cabecera = null;
        this.clase = clase;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                System.out.print(tmp.getDato() + "\t");
                tmp = tmp.getSiguiente();
            }
        }
    }

    private void insertar(T dato) {
        NodoDato tmp = new NodoDato(dato, cabecera);
        cabecera = tmp;
    }

    public boolean insertarDato(T dato) {
        try {
            inserTarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public void eliminarNodoEspecifico(int pos) {
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            if (pos == 0) {
                extraer();
            } else {
                NodoDato aux = cabecera;
                for (int i = 0; i < pos; i++) {
                    aux = aux.getSiguiente();
                }

                NodoDato siguiente = aux.getSiguiente();
                aux.setSiguiente(siguiente.getSiguiente());
            }
        }
    }

    public T consultarDatoPos(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    public boolean modificarDatoPos(int pos, T data) {
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(data);
                return true;
            }
        }
        return false;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            NodoDato iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            NodoDato tmp = new NodoDato(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);

        }
    }

    private void inserTarFinal(T dato) {
        insertar(dato, tamanio() - 1);
    }
    
    public boolean EditarDato(T dato){
        if (!estaVacio()) {
            NodoDato presente = cabecera;
            while (presente != null){
                if (presente.getDato() == dato) {
                    presente.getDato();
                    presente.setDato(dato);
                    return true;
                }
                presente = presente.getSiguiente();
            }
        }
        return false;
    }
    
    public boolean EliminarDato(T dato){
        presente = cabecera;
        pasado = null;
        while (presente != null){
            if (presente.getDato() == dato) {
                if (presente == cabecera) {
                    cabecera = cabecera.getSiguiente();
                    return true;
                } else {
                    pasado.setSiguiente(presente.getSiguiente());
                    return true;
                }
            }
            pasado = presente;
            presente = presente.getSiguiente();
        }
        return false;   
    }

    /**
     * Ordenar por titulo
     *
     * @param field
     * @return
     */
    public ListaSimple<T> ordenar(String field) {
        try {
            ListaSimple<T> lista = this;
            for (int i = 0; i < lista.tamanio() - 1; i++) {
                int k = i;
                T t = this.consultarDatoPos(i);
                for (int j = i + 1; j < lista.tamanio(); j++) {
                    String valor = getValueField(getFlied(field), consultarDatoPos(j)).toString();
                    if (valor.toLowerCase().compareTo(getValueField(getFlied(field), t).toString().toLowerCase()) < 0) {
                        t = lista.consultarDatoPos(j);
                        k = j;
                    }
                }
                lista.modificarDatoPos(k, lista.consultarDatoPos(i));
                lista.modificarDatoPos(i, t);
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Error en lista ordenar " + e);
            e.printStackTrace();
            return new ListaSimple<>();
        }
    }

    public Field getFlied(String atribute_name) {
        Field field = null;
        for (Field aux : this.clase.getDeclaredFields()) {
            if (atribute_name.equalsIgnoreCase(aux.getName())) {
                field = aux;
                break;
            }
        }

        if (field == null) {
            for (Field aux : this.clase.getSuperclass().getDeclaredFields()) {
                if (atribute_name.equalsIgnoreCase(aux.getName())) {
                    field = aux;
                    break;
                }
            }
        }
        return field;
    }

    public Object getValueField(Field field, T dato) throws Exception {
        Method metodo = null;
        for (Method aux : this.clase.getDeclaredMethods()) {
            if (aux.getName().toLowerCase().equalsIgnoreCase("get" + field.getName().toLowerCase())) {
                metodo = null;
            }
        }
        System.out.println(dato);
        if (metodo == null) {
            for (Method aux : this.clase.getSuperclass().getDeclaredMethods()) {
                if (aux.getName().toLowerCase().equalsIgnoreCase("get" + field.getName().toLowerCase())) {
                    metodo = aux;
                }
            }
        }
        return metodo.invoke(dato);
    }

}
