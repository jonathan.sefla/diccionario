/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import Controlador.lista.ListaSimple;


public interface InterfazDao <T>{
    public void guardar(T dato) throws Exception;
    public void modificar(int pos, T dato) throws Exception;
    public ListaSimple<T> listar() throws Exception;
    public void Eliminar (int pos) throws Exception;
}
