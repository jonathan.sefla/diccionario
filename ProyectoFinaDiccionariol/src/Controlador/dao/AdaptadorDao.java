/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import Controlador.lista.ListaSimple;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;


public class AdaptadorDao<T> implements InterfazDao<T>{
    
    private Conexion conexion;
    private Class<T> clazz;
    private String data_dir;

    public AdaptadorDao(Class<T> clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        data_dir = Conexion.REPOSITORIO + File.separatorChar+clazz.getSimpleName()+".xml";
    }
        
    
    @Override
    public void guardar(T dato) throws Exception {                
        ListaSimple<T> listado = this.listar();
        listado.insertarDato(dato);
        this.conexion.getxStream().toXML(listado, new FileOutputStream(data_dir));
    }

    @Override
    public ListaSimple<T> listar() throws Exception {
        ListaSimple<T> lista = new ListaSimple();
        try {
            lista = (ListaSimple<T>) conexion.getxStream().fromXML(new FileReader(data_dir));
            lista.setClase(clazz);
            
        } catch (Exception e) {
            System.out.println("error en listar "+e);
        }
        return lista;
    }

     @Override
    public void modificar(int pos, T dato) throws Exception {
        try {
            ListaSimple<T> listado = this.listar();
            listado.modificarDatoPos(pos, dato);
            this.conexion.getxStream().toXML(listado, new FileOutputStream(data_dir));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    @Override
    public void Eliminar(int pos) throws Exception {
        ListaSimple<T> lista = this.listar();
        lista.eliminarNodoEspecifico(pos);
        this.conexion.getxStream().toXML(lista, new FileOutputStream(data_dir));
    }
     
}
