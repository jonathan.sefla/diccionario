/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import Controlador.lista.ListaSimple;
import Modelo.Persona;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Usuario
 */
public class PersonaDao extends AdaptadorDao<Persona> {

    private Persona persona;
    private Persona personaAux;
    private ListaSimple<Persona> listado;

    /**
     * Constructor de CuentaDao
     */
    public PersonaDao() {
        super(Persona.class);
    }

    /**
     * Encapsulamiento de persona (get)
     *
     * @return persona
     */
    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }
        return persona;
    }

    /**
     * Encapsulamiento de persona (set)
     *
     * @param persona
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * Permite guardar a cada persona, ya sea usuario o administrador
     * @return true cuando se haya guardado una persona 
     * y false cuando haya ocurrido un error
     */
    public Boolean guardar() {
        try {
            this.persona.setId(listar().tamanio() + 1);
            guardar(this.persona);
            return true;
        } catch (Exception e) {
            System.out.println("error " + e);
            return false;
        }
    }
    
    /**
     * Permite modificar cada persona por su posición
     * @param nuevaPersona 
     * @return true cuando se haya modificado una persona
     * y false cuando haya ocurrido un error
     */
    public Boolean modificar(Persona nuevaPersona) {
        try {
            int pos = posicion(persona);
            modificar(pos, nuevaPersona);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Permite identificar la posición de una persona
     * @param dato sirve para saber el nombre de una persona
     * @return la posición donde se encuentra esta persona
     */
    private int posicion(Persona dato) {
        int pos = -1;
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 1; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                if (dato.getNombre().equalsIgnoreCase(aux.getNombre().toLowerCase())) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }

    /**
     * Permite buscar una sola persona por medio de su nombre
     * @param nombrePersona nos sirve para comparar si el nombre ingresado es igual
     * @return la persona buscada, para ser modificada 
     */ 
    public Persona buscarUnaPersona(String nombrePersona) {
        Persona persona = null;
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                if (nombrePersona.equalsIgnoreCase(aux.getNombre())) {//busqueda atomica
                    persona = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return persona;
    }
    
    /**
     * Permite hacer la busqueda de una persona solamente con el rol Administrador
     * @param personaBuscar nos sirve para comparar si el nombre ingresado es igual
     * @return la persona buscada
     */
    public ListaSimple<Persona> buscar(String personaBuscar) {
        ListaSimple<Persona> listado = new ListaSimple<>();
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                if (aux.getNombre().toUpperCase().contains(personaBuscar.toUpperCase()) && aux.getRol().equals("Administrador")) {
                    listado.insertarDato(aux);
                }
            }
        } catch (Exception e) {
        }
        return listado;
    }
    
    /**
     * Permite eliminar por su posición
     * @param persona sirve para identificar a la persona 
     * @return true cuando haya eliminado una persona de una posición correcta
     * y false cuando no haya posición
     */
    public boolean eliminar(Persona persona) {
        try {
            int pos = posicion(persona);
            Eliminar(pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error" + e);
            return false;
        }
    }
    
    /**
     * Permite autentificar a los usuarios de diversos roles
     * @param usuario verifica si el usuario es correcto
     * @param password verifica si la contraseña es correcta
     * @return el rol del usuario que ingresa al sistema
     */
    public String Autentificar(String usuario, String password) {
        String rol = "";
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documentoXml = builder.parse(new File("datos/Persona.xml"));
            documentoXml.getDocumentElement().normalize();
            String consultaUsuario = "//user";
            String consultaClave = "//clave";
            String consultaRol = "//rol";

            XPath xpath = XPathFactory.newInstance().newXPath();

            NodeList nodos = (NodeList) xpath.evaluate(consultaUsuario, documentoXml, XPathConstants.NODESET);
            NodeList nodos2 = (NodeList) xpath.evaluate(consultaClave, documentoXml, XPathConstants.NODESET);
            NodeList nodos3 = (NodeList) xpath.evaluate(consultaRol, documentoXml, XPathConstants.NODESET);

            for (int i = 0; i < nodos.getLength(); i++) {
                if (nodos.item(0).getTextContent().equals(usuario) && nodos2.item(0).getTextContent().equals(password) && nodos3.item(0).getTextContent().equals("Administrador Principal")) {
                    rol = "adminP";
                } else if (nodos.item(i).getTextContent().equals(usuario) && nodos2.item(i).getTextContent().equals(password) && nodos3.item(i).getTextContent().equals("Administrador")) {
                    rol = "admin";
                } else if (nodos.item(i).getTextContent().equals(usuario) && nodos2.item(i).getTextContent().equals(password) && nodos3.item(i).getTextContent().equals("Usuario")) {
                    rol = "user";
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            System.out.println("erro al leer archivo " + ex);
        }
        return rol;
    }
    
    /**
     * Permite obtener el listado de usuarios solamente del rol Administrador para presentarlo en la tabla
     * @return una lista auxiliar
     */
    public ListaSimple<Persona> getListado() {
        try {
            listado = listar();
            ListaSimple<Persona> aux = new ListaSimple<>();
            for (int i = 0; i < listado.tamanio(); i++) {
                if (listado.consultarDatoPos(i).getRol().equals("Administrador")) {
                    aux.insertarDato(listado.consultarDatoPos(i));
                }
            }
            return aux;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return the personaAux
     */
    public Persona getPersonaAux() {
        if (personaAux == null) {
            personaAux = new Persona();
        }
        return personaAux;
    }

    public void setPersonaAux(Persona persona) {
        this.personaAux = persona;
    }
    
    /**
     * Permite verificar si un nombre de usuario es repetido cuando una persona normal se registra en el sistema
     * @param nombreUsuario se compara con el nombre de usuario que registre
     * @return true cuando ya existe ese nombre de usuario y false no pasa nada
     */
    public boolean  UsuarioRepetido (String nombreUsuario) {
        try {
            ListaSimple<Persona> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Persona aux = lista.consultarDatoPos(i);
                if (nombreUsuario.equalsIgnoreCase(aux.getCuenta().getUser())) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

}
