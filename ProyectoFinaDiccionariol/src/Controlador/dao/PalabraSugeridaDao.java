/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import Controlador.lista.ListaSimple;
import Modelo.PalabraSugerida;


public class PalabraSugeridaDao extends AdaptadorDao<PalabraSugerida> {

    private PalabraSugerida palabraSugerida;

    public PalabraSugeridaDao() {
        super(PalabraSugerida.class);
    }

    /**
     * *
     * Devuelve la variable palabraAux de palabraSugerida
     *
     * @return
     */
    public PalabraSugerida getPalabraSugerida() {
        if (palabraSugerida == null) {
            palabraSugerida = new PalabraSugerida();
        }
        return palabraSugerida;
    }

    /**
     * *
     * Setea la variable palabraSugerida de PalabraDao
     *
     * @param palabraSugerida
     */
    public void setPalabraSugerida(PalabraSugerida palabraSugerida) {
        this.palabraSugerida = palabraSugerida;
    }

    /**
     * *
     * Método para guardar una palabra sugerida
     *
     * @return
     */
    public Boolean guardar() {
        try {
            this.palabraSugerida.setId(listar().tamanio() + 1);
            guardar(this.palabraSugerida);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /***
     * Método que devuelve la posición de una palabra sugerida en el archivo xml
     * @param dato Palabra de la que se desea la posición
     * @return Posición de la palabra sugerida
     */
    private int posicion(PalabraSugerida dato) {
        int pos = -1;
        try {
            ListaSimple<PalabraSugerida> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                PalabraSugerida aux = lista.consultarDatoPos(pos);
                if (dato.getPalabra().equalsIgnoreCase(aux.getPalabra().toLowerCase())) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }
    
    /***
     * Método que permite eliminar una palabra sugerida
     * @param palabrasugerida Palabra que se desea eliminar
     * @return True = Si se a eliminado correctamente
     */
    public boolean eliminar(PalabraSugerida palabrasugerida) {
        try {
            int pos = posicion(palabrasugerida);
            Eliminar(pos);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    /**
     * Permite verificar si una palabra sugerida es repetida cuando una persona normal la registra en el sistema
     * @param nombrePalabra se compara con todos el nombre de palabra que registre
     * @return true cuando ya existe ese nombre de palabra y false no pasa nada
     */
    public boolean PalabraRepetida (String nombrePalabra) {
        try {
            ListaSimple<PalabraSugerida> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                PalabraSugerida aux = lista.consultarDatoPos(i);
                if (nombrePalabra.equalsIgnoreCase(aux.getPalabra())) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    
}
