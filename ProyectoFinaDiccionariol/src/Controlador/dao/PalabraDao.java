/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import Controlador.lista.ListaSimple;
import Modelo.Palabra;
import java.io.BufferedWriter;
import java.io.FileWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author DELL
 */
public class PalabraDao extends AdaptadorDao<Palabra> {

    private Palabra palabra;
    private Palabra palabraAux;

    public PalabraDao() {
        super(Palabra.class);
    }

    /**
     * *
     * Setea la variable palabra de PalabraDao
     *
     * @param palabra
     */
    public void setPalabra(Palabra palabra) {
        this.palabra = palabra;
    }

    /**
     * *
     * Devuelve la variable palabra de PalabraDao
     *
     * @return
     */
    public Palabra getPalabra() {
        if (palabra == null) {
            palabra = new Palabra();
        }
        return palabra;
    }

    /**
     * *
     * Setea la variable palabraAux de PalabraDao
     *
     * @param palabra
     */
    public void setPalabraAux(Palabra palabra) {
        this.palabraAux = palabra;
    }

    /**
     * *
     * Devuelve la variable palabraAux de PalabraDao
     *
     * @return
     */
    public Palabra getPalabraAux() {
        if (palabraAux == null) {
            palabraAux = new Palabra();
        }
        return palabraAux;
    }

    /**
     * *
     * Método para guardar una nueva palabra
     *
     * @return
     */
    public Boolean guardar() {
        try {
            guardar(this.palabra);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * *
     * Método para modificar una palabra ya antes registrada
     *
     * @param pala Palabra con los cambios realizados
     * @return
     */
    public Boolean modificar(Palabra pala) {
        try {
            int pos = posicion(palabra);
            modificar(pos, pala);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * *
     * Método para ordenar las
     *
     * @return Lista Ordenada
     */
    public ListaSimple<Palabra> ordenar() {
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio() - 1; i++) {
                int k = i;
                Palabra t = lista.consultarDatoPos(i);
                for (int j = i + 1; j < lista.tamanio(); j++) {
                    if (lista.consultarDatoPos(j).getNombrePalabra().toLowerCase().compareTo(t.getNombrePalabra().toLowerCase()) < 0) {
                        t = lista.consultarDatoPos(j);
                        k = j;
                    }
                }
                lista.modificarDatoPos(k, lista.consultarDatoPos(i));
                lista.modificarDatoPos(i, t);
            }
            return lista;
        } catch (Exception e) {
            return new ListaSimple<>();
        }
    }

    /**
     * *
     * Método que devuelve la posición de una palabra en el archivo xml
     *
     * @param dato Palabra de la que se desea la posición
     * @return Posición de la palabra
     */
    private int posicion(Palabra dato) {
        int pos = -1;
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Palabra aux = lista.consultarDatoPos(i);
                if (dato.getNombrePalabra().equalsIgnoreCase(aux.getNombrePalabra().toLowerCase())) {
                    pos = i;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return pos;
    }

    /**
     * *
     * Método que busca y devuelve una sola palabra
     *
     * @param nombrePalabra Nombre de la palabra que se desea buscar
     * @return Palabra encontrada
     */
    public Palabra buscarUnaPalabra(String nombrePalabra) {
        Palabra palabra = null;
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Palabra aux = lista.consultarDatoPos(i);
                if (nombrePalabra.equalsIgnoreCase(aux.getNombrePalabra())) {//busqueda atomica
                    palabra = aux;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return palabra;
    }

    /**
     * *
     * Método que busca y devuelve una lista con resultado de una palabra por su
     * nombre
     *
     * @param palabraaBuscar Nombre de la palabra a buscar
     * @return Lista con resultados de la busqueda
     */
    public ListaSimple<Palabra> buscar(String palabraaBuscar) {
        ListaSimple<Palabra> listado = new ListaSimple<>();
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Palabra aux = lista.consultarDatoPos(i);

                if (aux.getNombrePalabra().toUpperCase().contains(palabraaBuscar.toUpperCase())) {
                    listado.insertarDato(aux);
                }

            }

        } catch (Exception e) {
        }
        return listado;
    }

    /**
     * *
     * Método que busca y devuelve una lista con resultado de una palabra por su
     * índice
     *
     * @param indice Indice de la palabra a buscar
     * @return Lista con resultados de la busqueda
     */
    public ListaSimple<Palabra> buscarPorIndice(String indice) {
        char letra = indice.toLowerCase().charAt(0);

        ListaSimple<Palabra> listado = new ListaSimple<>();
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Palabra aux = lista.consultarDatoPos(i);
                char auxC = aux.getNombrePalabra().toLowerCase().charAt(0);

                if (Character.compare(letra, auxC) == 0) {
                    listado.insertarDato(aux);
                }
            }
        } catch (Exception e) {
        }
        return listado;
    }

    /**
     * *
     * Método que verifica si una palabra está repetida
     *
     * @param nombrePalabra Nombre de la palabra a verificar
     * @return True = si la palabra ya esta registrada False = no existe en el
     * registro
     */
    public boolean PalabraRepetida(String nombrePalabra) {
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                Palabra aux = lista.consultarDatoPos(i);
                if (nombrePalabra.equalsIgnoreCase(aux.getNombrePalabra())) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * *
     * Método para Actualizar el archivo de busqueda de palabras por voz
     */
    public void ActualizarArchivoBuscadaPorVoz() {
        if (!ActualizarArchivoSimpleGrammarES2(formato1ArchivoSimpleGrammarES2(), formato2ArchivoSimpleGrammarES2())) {
            JOptionPane.showMessageDialog(null, "No se pudo actualizar el archivo de Busqueda por voz", "Error", 0);
        }
    }

    /**
     * *
     * Método para dar el primer formato al archivo SimpleGrammarES2
     *
     * @return Primer formato
     */
    private ListaSimple<String> formato1ArchivoSimpleGrammarES2() {
        ListaSimple<String> formato = new ListaSimple<>();
        try {
            int lista = listar().tamanio();
            for (int i = 0; i < lista; i++) {
                formato.insertarDato("[<dato" + i + ">]");
                if (i == lista - 1) {
                    String aux = formato.consultarDatoPos(i);
                    aux = aux + ";";
                    formato.modificarDatoPos(i, aux);
                }
            }

        } catch (Exception ex) {
            return new ListaSimple<>();
        }
        return formato;
    }

    /**
     * *
     * Método para dar el segundo formato al archivo SimpleGrammarES2
     *
     * @return Segundo formato
     */
    private ListaSimple<String> formato2ArchivoSimpleGrammarES2() {
        ListaSimple<String> formato = new ListaSimple<>();
        try {
            ListaSimple<Palabra> lista = listar();
            for (int i = 0; i < lista.tamanio(); i++) {
                formato.insertarDato("<dato" + i + ">=" + lista.consultarDatoPos(i).getNombrePalabra() + ";");
            }
        } catch (Exception ex) {
            return new ListaSimple<>();
        }
        return formato;
    }

    /**
     * *
     * Método para actualizar el archivo SimpleGrammarES2
     *
     * @param formato1 Lista con el primer formato del archivo
     * @param formato2 Lista con el segundo formato del archivo
     * @return True = Archivo actualizado exitosamente
     */
    private boolean ActualizarArchivoSimpleGrammarES2(ListaSimple<String> formato1, ListaSimple<String> formato2) {
        String s1 = "#JSGF V1.0;";
        String s2 = "grammar sentence;";
        String s3 = "public <sentence> =";

        try {

            BufferedWriter out = null;
            out = new BufferedWriter(new FileWriter("datos/SimpleGrammarES2.txt", false));
            // datos que se va a guardar dentro del archivo
            out.write(s1 + "\n");
            out.write(s2 + "\n\n");
            out.write(s3 + "\n");

            for (int i = 0; i < formato1.tamanio(); i++) {
                out.write(formato1.consultarDatoPos(i) + "\n");
            }

            out.write("\n");

            for (int j = 0; j < formato2.tamanio(); j++) {
                out.write(formato2.consultarDatoPos(j) + "\n");
            }
            out.write("\n");
            out.flush();
            out.close();
            return true;

        } catch (Exception e) {
            return false;
        }
    }
}
