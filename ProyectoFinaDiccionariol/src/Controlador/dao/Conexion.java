/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controlador.dao;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;


public class Conexion {
    public static final String REPOSITORIO = "datos";
    private XStream xStream;
    
    public Conexion() {
        xStream = new XStream(new JettisonMappedXmlDriver());//DomDriver
        xStream = new XStream(new DomDriver());
        xStream.setMode(XStream.NO_REFERENCES);
    }

    public XStream getxStream() {
        return xStream;
    }
    
}
