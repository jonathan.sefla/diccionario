/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controladores;

import Controlador.lista.ListaSimple;


public class ControladorSinonimos {
    String sinonimo = new String();
    ListaSimple<String> sinonimos = new ListaSimple<>();

    public void guardarSinonimo(String sinonimo){
        sinonimos.insertarDato(sinonimo);
    }
    
    public String getSinonimo() {
        if(sinonimo == null)
            sinonimo = new String();
        return sinonimo;
    }

    public void setSinonimo(String sinonimo) {
        this.sinonimo = sinonimo;
    }

    public ListaSimple<String> getSinonimos() {
        return sinonimos;
    }

    public void setSinonimos(ListaSimple<String> sinonimos) {
        this.sinonimos = sinonimos;
    }
    
}
