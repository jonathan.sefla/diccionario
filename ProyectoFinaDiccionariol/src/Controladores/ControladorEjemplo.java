/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controladores;

import Controlador.lista.ListaSimple;
import Modelo.Ejemplo;


public class ControladorEjemplo {
    private Ejemplo ejemplo = new Ejemplo();
    private ListaSimple<Ejemplo> lista = new ListaSimple<>();
    
    public void guardarEjemplos(Ejemplo ejemplo){
        lista.insertarDato(ejemplo);
    }
    
    public void setEjemplo(Ejemplo ejemplo) {
        this.ejemplo = ejemplo;
    }
    
    public Ejemplo getEjemplo() {
        if(ejemplo == null)
            ejemplo = new Ejemplo();
        return ejemplo;
    }

    public ListaSimple<Ejemplo> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Ejemplo> lista) {
        this.lista = lista;
    }
    
    public Ejemplo buscarEjemplo(String dato){
        Ejemplo ejemplo = null;
            for (int i = 0; i < lista.tamanio(); i++) {
                Ejemplo aux = lista.consultarDatoPos(i);
                if (dato.equals(aux.getDescripcion())) {
                    ejemplo = aux;
                    break;
                }
            }
        return ejemplo;
    }
    
}
