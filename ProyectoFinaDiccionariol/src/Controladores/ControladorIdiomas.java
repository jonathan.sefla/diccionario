/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controladores;

import Controlador.lista.ListaSimple;
import Modelo.Idioma;
import Modelo.enums.Idiomas;
import Modelo.enums.SiglasIdiomas;


public class ControladorIdiomas {

    private Idioma idioma = new Idioma();
    private ListaSimple<Idioma> lista = new ListaSimple<>();

    public void agregarIdiomas() {
        int i = 0;
        for (Idiomas doc : Idiomas.values()) {
            Idioma aux = new Idioma();
            lista.insertarDato(aux);
            lista.consultarDatoPos(i).setNombreIdioma(String.valueOf(doc));
            i++;
        }
        
        i = 0;
        for (SiglasIdiomas doc : SiglasIdiomas.values()) {
            lista.consultarDatoPos(i).setSiglas(doc.toString());
            i++;
        }
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    public Idioma getIdioma() {
        if (idioma == null) {
            idioma = new Idioma();
        }
        return idioma;
    }

    public ListaSimple<Idioma> getLista() {
        return lista;
    }

}
