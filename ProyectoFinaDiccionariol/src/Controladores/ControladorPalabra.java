/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Controladores;

import java.io.FileReader;
import java.util.Locale;
import java.util.Objects;
import javax.speech.Central;
import javax.speech.EngineModeDesc;
import javax.speech.recognition.DictationGrammar;
import javax.speech.recognition.Recognizer;
import javax.speech.recognition.Result;
import javax.speech.recognition.ResultAdapter;
import javax.speech.recognition.ResultEvent;
import javax.speech.recognition.ResultToken;
import javax.speech.recognition.RuleGrammar;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.Voice;
import javax.swing.JOptionPane;

public class ControladorPalabra extends ResultAdapter {

    private Recognizer recognizer;
    private String gst;

    /**
     * *
     * El evento RESULT_ACCEPTED se emite después de que el reconocedor emite un
     * evento RECOGNIZER_SUSPENDED para pasar del estado PROCESSING al estado
     * SUSPENDED. Cualquier cambio realizado en las gramáticas o en el estado
     * habilitado de las gramáticas durante el procesamiento del evento
     * RESULT_ACCEPTED se confirma automáticamente una vez que todos los
     * ResultListeners han procesado el evento RESULT_ACCEPTED. Una vez que se
     * han confirmado esos cambios, el Recognizer vuelve al estado LISTENING con
     * un evento CHANGES_COMMITTED. No se requiere una llamada a commitChanges.
     * (Excepto, si hay una llamada a suspender sin una llamada posterior a
     * commitChanges, el Recognizer pospone la confirmación hasta que se recibe
     * la llamada commitChanges).
     *
     * @param re
     */
    @Override
    public void resultAccepted(ResultEvent re) {

        try {
            //Guardando el resultado de la palabra que coincide con la gramatica cargada
            Result res = (Result) (re.getSource());

            //Almacenando en una array todas las palabras que mejor se asemejen a la gramatica cargada
            ResultToken tokens[] = res.getBestTokens();

            //Recorriendo todos los resultados 
            for (int i = 0; i < tokens.length; i++) {
                //Obteniendo las palabras
                gst = tokens[i].getSpokenText();

            }
            //Presentando el resultado en el frame
            resultado(gst);
            //Detener el reconocedor 
            recognizer.deallocate();

        } catch (Exception ex) {
        }
    }

    /**
     * *
     * Metodo para inicializar la busqueda por voz
     */
    public void busquedaPorVoz() {
        try {
            //Creando un reconocedor que admita el español, root representa el idioma configurado en la computadora
            recognizer = Central.createRecognizer(new EngineModeDesc(Locale.ROOT));
            //Iniciando el reconocedor
            recognizer.allocate();

            //Cargando la grammatica de un archivo y habilitandola
            FileReader diccionario = new FileReader("datos/SimpleGrammarES2.txt");
            RuleGrammar rg = recognizer.loadJSGF(diccionario);
            rg.setEnabled(true);
            //Agregando al listener para obtener resultados
            recognizer.addResultListener(new ControladorPalabra());
            //confirmando la gramatica
            recognizer.commitChanges();
            //Solicitando el enfoque e iniciando el Listener 
            recognizer.requestFocus();
            //Deteniendo el programa 2 s, para escuchar la palabra del usuario
            Thread.sleep(2000);
            //Pausando el reconocedor
            recognizer.pause();

        } catch (Exception e) {
            System.out.println("Exception en " + e);
        }

    }

    /**
     * Metodo para presentar el resultado de la palabra buscada mediante la voz,
     * en el frame principal,
     *
     * @param resultado
     */
    public void resultado(String resultado) {
        Vista.FrmPrincipal.txtPalabraABuscar.setText(gst);

    }
    /***
     * Metodo que lee la palabra buscada por el usuario
     * @param palabra 
     */
    public void leerPalabra(String palabra){
        try {

            String say = palabra;

            SynthesizerModeDesc required = new SynthesizerModeDesc();
            
            required.setLocale(Locale.ROOT);

            Voice voice = new Voice(null, Voice.GENDER_FEMALE, Voice.GENDER_FEMALE, null);

            required.addVoice(voice);

            Synthesizer synth = Central.createSynthesizer(null);

            synth.allocate();
            synth.resume();

            synth.speakPlainText(say, null);

            synth.waitEngineState(Synthesizer.QUEUE_EMPTY);
            synth.deallocate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
