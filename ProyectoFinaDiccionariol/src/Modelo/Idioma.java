/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Modelo;


public class Idioma {
    private String nombreIdioma;
    private String siglas;

    /**
     * @return the nombreIdioma
     */
    public String getNombreIdioma() {
        return nombreIdioma;
    }

    /**
     * @param nombreIdioma the nombreIdioma to set
     */
    public void setNombreIdioma(String nombreIdioma) {
        this.nombreIdioma = nombreIdioma;
    }

    /**
     * @return the siglas
     */
    public String getSiglas() {
        return siglas;
    }

    /**
     * @param siglas the siglas to set
     */
    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

 
}
