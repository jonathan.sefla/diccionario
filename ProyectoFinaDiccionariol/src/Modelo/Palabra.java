/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Modelo;

import Controlador.lista.ListaSimple;
import java.io.Serializable;


public class Palabra implements Serializable{
    
    private ListaSimple <Idioma> idiomas;
    private ListaSimple <Ejemplo> ejemplos;
    private String nombrePalabra;
    private String traduccion;
    private String significado;
    private ListaSimple<String> sinonimo;
    private String imagen;
        
    /**
     * @return the nombrePalabra
     */
    public String getNombrePalabra() {
        return nombrePalabra;
    }

    /**
     * @param nombrePalabra the nombrePalabra to set
     */
    public void setNombrePalabra(String nombrePalabra) {
        this.nombrePalabra = nombrePalabra;
    }
    
    /**
     * @return the significado
     */
    public String getSignificado() {
        return significado;
    }

    /**
     * @param significado the significado to set
     */
    public void setSignificado(String significado) {
        this.significado = significado;
    }

    /**
     * @return the idiomas
     */
    public ListaSimple <Idioma> getIdiomas() {
        return idiomas;
    }

    /**
     * @param idiomas the idiomas to set
     */
    public void setIdiomas(ListaSimple <Idioma> idiomas) {
        this.idiomas = idiomas;
    }

    /**
     * @return the ejemplos
     */
    public ListaSimple <Ejemplo> getEjemplos() {
        return ejemplos;
    }

    /**
     * @param ejemplos the ejemplos to set
     */
    public void setEjemplos(ListaSimple <Ejemplo> ejemplos) {
        this.ejemplos = ejemplos;
    }

    /**
     * @return the sinonimo
     */
    public ListaSimple<String> getSinonimo() {
        return sinonimo;
    }

    /**
     * @param sinonimo the sinonimo to set
     */
    public void setSinonimo(ListaSimple<String> sinonimo) {
        this.sinonimo = sinonimo;
    }

    /**
     * @return the imagen
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * @param ruta the imagen to set
     */
    public void setImagen(String ruta) {
        this.imagen = ruta;
    }

    @Override
    public String toString() {
        return getNombrePalabra();
    }

    /**
     * @return the traduccion
     */
    public String getTraduccion() {
        return traduccion;
    }

    /**
     * @param traduccion the traduccion to set
     */
    public void setTraduccion(String traduccion) {
        this.traduccion = traduccion;
    }

}
