/*
 * Proyecto Final: Estructura de Datos
 * Diccionario
 * JACE - JDSM - ADUA - AAVJ
 */
package Modelo.enums;


public enum Idiomas {
    Espanol, Ingles;
}
